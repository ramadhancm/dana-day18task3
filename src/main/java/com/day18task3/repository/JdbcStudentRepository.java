package com.day18task3.repository;

import com.day18task3.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class JdbcStudentRepository implements StudentRepository {

    // Spring Boot will create and configure DataSource and JdbcTemplate
    // To use it, just @Autowired
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public int count() {
        return jdbcTemplate
                .queryForObject("select count(*) from students", Integer.class);
    }

    @Override
    public int insert(Student student) {
        return jdbcTemplate.update(
                "insert into students (name, address, major, attendance) values(?,?,?,?)",
                    student.getName(), student.getAddress(), student.getMajor(), "0000-00-00");
    }

    @Override
    public int updateAttendance(int id, String attendance) {
        return jdbcTemplate.update(
                "update students set attendance = ? where id = ?",
                attendance, id);
    }

    @Override
    public int deleteById(int id) {
        return jdbcTemplate.update(
                "delete from students where id = ?",
                id);
    }

    @Override
    public List<Student> findAll() {
        return jdbcTemplate.query(
                "select * from students",
                (rs, rowNum) ->
                        new Student(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("address"),
                                rs.getString("major"),
                                rs.getString("attendance")
                        )
        );
    }

    @Override
    public Optional<Student> findById(int id) {
        return jdbcTemplate.queryForObject(
                "select * from students where id = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        Optional.of(new Student(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("address"),
                                rs.getString("major"),
                                rs.getString("attendance")
                        ))
        );
    }
}
