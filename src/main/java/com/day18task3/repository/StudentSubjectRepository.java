package com.day18task3.repository;

import com.day18task3.model.Student;
import com.day18task3.model.StudentSubject;

import java.util.List;
import java.util.Optional;

public interface StudentSubjectRepository {
    int count(int studentid);
    int insert(StudentSubject studentsubject);
    int updateAttendance(int id, String attendance);
    int deleteById(int id);

    Optional<StudentSubject> findById(int id);
    List<StudentSubject> findAll();
    List<StudentSubject> findByStudentId(int studentid);
    List<StudentSubject> findBySubjectId(int subjectid);
}
