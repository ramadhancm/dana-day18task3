package com.day18task3.model;

public class Student {
    private int id;

    private String name;

    private String address;

    private String major;

    private String attendance;

    public Student(int id, String name, String address, String major, String attendance) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.major = major;
        this.attendance = attendance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }
}
